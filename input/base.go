package input

type Syllable struct {
	Text               string
	FromTick, TillTick int
}

type Word []Syllable

type Line []Word

type Voice []Line

type Song struct {
	Voices []Voice
}

type voiceBuilder struct {
	word Word
	line Line
	voice Voice
}

func (u *voiceBuilder) AppendSyllable(s Syllable) *Syllable {
	u.word = append(u.word, s)
	return &u.word[len(u.word)-1]
}

func (u *voiceBuilder) FinishWord() {
	if len(u.word) > 0 {
		u.line = append(u.line, u.word)
		u.word = Word{}
	}
}

func (u *voiceBuilder) FinishLine() {
	u.FinishWord()
	if len(u.line) > 0 {
		u.voice = append(u.voice, u.line)
		u.line = Line{}
	}
}

func (u *voiceBuilder) Finalize() Voice {
	u.FinishLine()
	return u.voice
}
