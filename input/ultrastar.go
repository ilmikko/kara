package input

import (
	"fmt"
	"strconv"
	"strings"
)

func ParseUltraStar(data string) (Song, error) {
	lines := strings.Split(strings.Replace(data, "\r", "", -1), "\n")
	tags := map[string]string{}
	for _, l := range lines {
		if strings.HasPrefix(l, "#") {
			sp := strings.Split(l[1:], ":")
			tags[sp[0]] = sp[1]
		}
	}
	bpm, err := strconv.ParseFloat(strings.Replace(tags["BPM"], ",", ".", -1), 32)
	if err != nil {
		return Song{}, fmt.Errorf("failed to parse BPM: %v", err)
	}
	bpm *= 4
	gap, err := strconv.ParseFloat(tags["GAP"], 32)
	if err != nil {
		return Song{}, fmt.Errorf("failed to parse GAP: %v", err)
	}
	relative := (tags["RELATIVE"] == "YES")
	var vb voiceBuilder
	var previousSyllable *Syllable
	var voices []Voice
	var absoluteLineOffset int
	for i, l := range lines {
		if strings.HasPrefix(l, ":") || strings.HasPrefix(l, "*") || strings.HasPrefix(l, "F") {
			sp := strings.SplitN(l, " ", 5)
			f, err := strconv.ParseFloat(sp[1], 32)
			if err != nil {
				return Song{}, fmt.Errorf("failed to parse line %d: %q: %v", i, l, err)
			}
			d, err := strconv.ParseFloat(sp[2], 32)
			if err != nil {
				return Song{}, fmt.Errorf("failed to parse line %d: %q: %v", i, l, err)
			}
			fromTick := int((gap + (1000*(absoluteLineOffset+f))*60/bpm) / 3.333)
			tillTick := int((gap + (1000*(absoluteLineOffset+f+d))*60/bpm) / 3.333)
			if sp[4] == "~" {
				// The UltraStar format puts in a "~" syllable if one syllable has two different pitches.
				// We don't care for pitches, so just merge them together.
				previousSyllable.TillTick = tillTick
				continue
			}
			if strings.HasPrefix(sp[4], " ") {
				vb.FinishWord()
				sp[4] = strings.TrimLeft(sp[4], " ")
			}
			sb := Syllable{
				Text:     sp[4],
				FromTick: fromTick,
				TillTick: tillTick,
			}
			previousSyllable = vb.AppendSyllable(sb)
			if strings.HasSuffix(previousSyllable.Text, " ") {
				previousSyllable.Text = strings.TrimRight(previousSyllable.Text, " ")
				vb.FinishWord()
			}
		} else if strings.HasPrefix(l, "-") {
			vb.FinishLine()
			// Format: - <hide previous line at this beat> [start next line at this beat]
			// Note that the latter is optional, and we just default it to the former.
			sp := strings.Split(l, " ")
			eol, err := strconv.ParseFloat(sp[1], 32)
			if err != nil {
				return Song{}, fmt.Errorf("failed to parse line %d: %q: %v", i, l, err)
			}
			sol := eol
			if len(sp) > 3 && sp[2] != "" {
				sol, err = strconv.ParseFloat(sp[2], 32)
				if err != nil {
					return Song{}, fmt.Errorf("failed to parse line %d: %q: %v", i, l, err)
				}
			}
			if relative {
				absoluteLineOffset = sol
			}
		} else if strings.HasPrefix(l, "P") {
			v := vb.Finalize()
			if len(v) > 0 {
				voices = append(voices, v)
			}
			vb = voiceBuilder{}
		}
	}
	voices = append(voices, vb.Finalize())
	return Song{Voices: voices}, nil
}
