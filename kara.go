package main

import (
	"flag"
	"io/ioutil"
	"os"

	"gitlab.com/ilmikko/kara/input"
	"gitlab.com/ilmikko/kara/render"
)

var (
	inputFile = flag.String("input", "songs/code-in-go.ustr", "Name of the input file")
)

func main() {
	flag.Parse()

	ustr, err := ioutil.ReadFile(*inputFile)
	if err != nil {
		panic(err)
	}

	song, err := input.ParseUltraStar(string(ustr))
	if err != nil {
		panic(err)
	}

	if err := render.Render(song, os.Stdout); err != nil {
		panic(err)
	}
}
