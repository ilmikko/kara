package font

func GetChar(c rune) []uint8 {
	var idx int
	if int(c) >= len(asciiToIndex) {
		idx = 12
	} else {
		idx = asciiToIndex[c]
	}
	return fontData[idx : idx+12]
}
