package main

import (
	"bufio"
	"os"

	"gitlab.com/ilmikko/kara/instructions"
	"gitlab.com/ilmikko/kara/render"
	"gitlab.com/ilmikko/kara/scheduler"
)

func main() {
	bg := byte(0)
	fg := byte(1)

	palette := []instructions.RGB{
		{0x00, 0x00, 0x00},
		{0x3F, 0x3F, 0x3F},
		{0x3F, 0x00, 0x00},
		{0x3F, 0x3F, 0x00},
		{0x00, 0x3F, 0x3F},
		{0x3F, 0x00, 0x3F},
		{0x07, 0x00, 0x3F},
		{0x3F, 0x00, 0x07},
	}

	sched := scheduler.New()

	sched.AddAtStart(instructions.LoadColorTable{
		High:   false,
		Colors: palette,
	})
	sched.AddAtStart(instructions.LoadColorTable{
		High:   true,
		Colors: palette,
	})

	sched.AddAtStart(instructions.MemoryPreset{bg})
	sched.AddAtStart(instructions.BorderPreset{fg})

	// TODO: We don't want to keep track of t or y
	t := 16
	y := 0.0

	// TODO: Abstract this away to input/
	reader := bufio.NewReader(os.Stdin)

	for {
		line, err := reader.ReadString('\n')
		if err != nil {
			break
		}

		instructions := render.Line(line, 0, y, 2)
		y += 2

		for _, i := range instructions {
			sched.Add(i, t-5, t+5)
			t += 1
		}
		t += 100
	}

	out, err := sched.Schedule()
	if err != nil {
		panic(err)
	}
	for _, i := range out {
		if _, err := os.Stdout.Write(instructions.ToPacket(i)); err != nil {
			panic(err)
		}
	}
}
