package scheduler

import (
	"errors"
	"fmt"
	"sort"

	"gitlab.com/ilmikko/kara/instructions"
	"gitlab.com/ilmikko/kara/optimizer"
)

type queueEntry struct {
	latest      int
	instruction instructions.Instruction
}

func New() *Scheduler {
	return &Scheduler{
		byEarliest: map[int][]*queueEntry{},
	}
}

type Scheduler struct {
	frames       int
	instructions int
	byEarliest   map[int][]*queueEntry
	atStart      int
}

func (s *Scheduler) Add(i instructions.Instruction, earliest, latest int) {
	s.byEarliest[earliest] = append(s.byEarliest[earliest], &queueEntry{
		latest:      latest,
		instruction: i,
	})
	if latest+1 > s.frames {
		s.frames = latest + 1
	}
	s.instructions++
}

func (s *Scheduler) AddAtStart(i instructions.Instruction) {
	latest := s.atStart
	s.atStart++
	s.Add(i, 0, latest)
}

func (s *Scheduler) SetLength(frames int) {
	s.frames = frames
}

func (s *Scheduler) Schedule() ([]instructions.Instruction, error) {
	// Earliest deadline first scheduler.
	ret := make([]instructions.Instruction, s.frames)
	var schedulable []*queueEntry
	opt := optimizer.New()
outer:
	for i := 0; s.frames > i; i++ {
		if len(s.byEarliest[i]) > 0 {
			schedulable = append(schedulable, s.byEarliest[i]...)
			sort.SliceStable(schedulable, func(i, j int) bool {
				return schedulable[i].latest < schedulable[j].latest
			})
		}
		var qe *queueEntry
		for {
			if len(schedulable) == 0 {
				ret[i] = instructions.Pause{}
				// Shifting the first element off a slice in Go doens't actually free up the memory because it'll still be in the backing array. Reset the slice here so that the backing array gets freed.
				schedulable = nil
				continue outer
			}
			qe = schedulable[0]
			schedulable = schedulable[1:]
			if opt.Add(qe.instruction) {
				break
			}
			// The optimizer said this instruction is a noop. Skipping it.
		}
		if qe.latest < i {
			return nil, fmt.Errorf("failed to schedule all the deadlines (at #%d; trying to schedule %T (%#v))", i, qe.instruction, qe.instruction)
		}
		ret[i] = qe.instruction
	}
	if len(schedulable) > 0 {
		return nil, errors.New("failed to schedule all instructions (wrong SetLength call? Too restrictive timespans?)")
	}
	return ret, nil
}
