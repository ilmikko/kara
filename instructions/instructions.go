package instructions

type RGB struct {
	R, G, B uint8
}

type Instruction interface {
	Instruction() byte
	Data() []byte
}

type MemoryPreset struct {
	Color byte
}

func (i MemoryPreset) Instruction() byte {
	return 0x01
}

func (i MemoryPreset) Data() []byte {
	return padData([]byte{i.Color, 0x00})
}

type BorderPreset MemoryPreset

func (i BorderPreset) Instruction() byte {
	return 0x02
}

func (i BorderPreset) Data() []byte {
	return MemoryPreset(i).Data()
}

type TileBlock struct {
	Color0, Color1 byte
	Row, Column    byte
	TilePixels     []byte
}

func (i TileBlock) Instruction() byte {
	return 0x06
}

func (i TileBlock) Data() []byte {
	ret := make([]byte, 16)
	ret[0] = i.Color0
	ret[1] = i.Color1
	ret[2] = i.Row
	ret[3] = i.Column
	for i, tp := range i.TilePixels {
		ret[4+i] = tp >> 2
	}
	return ret
}

type TileXOR TileBlock

func (i TileXOR) Instruction() byte {
	return 0x26
}

func (i TileXOR) Data() []byte {
	return TileBlock(i).Data()
}

type HorizontalScrollDirection byte

const (
	NoHorizontalScroll HorizontalScrollDirection = 0
	ScrollRight        HorizontalScrollDirection = 1
	ScrollLeft         HorizontalScrollDirection = 2
)

type VerticalScrollDirection byte

const (
	NoVerticalScroll VerticalScrollDirection = 0
	ScrollDown       VerticalScrollDirection = 1
	ScrollUp         VerticalScrollDirection = 2
)

type Scroll struct {
	Color            byte
	Horizontal       HorizontalScrollDirection
	Vertical         VerticalScrollDirection
	HorizontalOffset byte
	VerticalOffset   byte
}

func (i Scroll) IsPreset() bool {
	return i.Color >= 16
}

func (i Scroll) Instruction() byte {
	if i.IsPreset() {
		return 0x18 // ScrollCopy
	}
	return 0x14 // ScrollPreset
}

func (i Scroll) Data() []byte {
	hScroll := i.HorizontalOffset | (byte(i.Horizontal) << 4)
	vScroll := i.VerticalOffset | (byte(i.Vertical) << 4)
	if i.IsPreset() {
		return []byte{0, hScroll, vScroll}
	}
	return []byte{i.Color, hScroll, vScroll}
}

type DefineTransparentColor struct {
	Color byte
}

func (i DefineTransparentColor) Instruction() byte {
	return 0x1C
}

func (i DefineTransparentColor) Data() []byte {
	return []byte{i.Color}
}

type LoadColorTable struct {
	High   bool
	Colors []RGB
}

func (i LoadColorTable) Instruction() byte {
	if i.High {
		return 0x1F
	}
	return 0x1E
}

func (i LoadColorTable) Data() []byte {
	ret := make([]byte, 0, 16)
	for _, c := range i.Colors {
		ret = append(ret, ((c.R&0xF)<<2)|((c.G&(0x3<<2))>>2), ((c.G&0x3)<<4)|(c.B&(0xF)))
	}
	return ret
}

type Pause struct{}

func (i Pause) Instruction() byte {
	return 0x00
}

func (i Pause) Data() []byte {
	return nil
}

var _ Instruction = MemoryPreset{}
var _ Instruction = BorderPreset{}
var _ Instruction = TileBlock{}
var _ Instruction = TileXOR{}
var _ Instruction = Scroll{}
var _ Instruction = DefineTransparentColor{}
var _ Instruction = LoadColorTable{}
var _ Instruction = Pause{}

func ToPacket(i Instruction) []byte {
	ret := make([]byte, 24)
	if _, ok := i.(Pause); !ok {
		ret[0] = 0x09
	}
	ret[1] = i.Instruction()
	// ret[2:3] == parityQ
	copy(ret[4:20], i.Data())
	// ret[20:23] == parityP
	return ret
}

func padData(i []byte) []byte {
	ret := make([]byte, 16)
	copy(ret, i)
	return ret
}
