package main

import (
	"bytes"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"

	"gitlab.com/ilmikko/kara/input"
	"gitlab.com/ilmikko/kara/render"
)

var (
	serveIP   = flag.String("sip", "localhost", "IP address to run server on.")
	servePort = flag.Int("sport", 8080, "Server port to use.")
)

func init() {
	flag.Parse()
}

func main() {
	http.Handle("/", http.FileServer(http.Dir("./web/")))
	http.HandleFunc("/upload", upload)
	serveAdd := fmt.Sprintf("%s:%d", *serveIP, *servePort)
	fmt.Printf("Running on: http://%s:%d", *serveIP, *servePort)
	http.ListenAndServe(serveAdd, nil)
}

func upload(w http.ResponseWriter, r *http.Request) {
	// the FormFile function takes in the POST input id file
	file, _, err := r.FormFile("file")
	if err != nil {
		fmt.Fprintln(w, err)
		return
	}
	defer file.Close()

	ustr, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Fprintln(w, err)
		return
	}

	song, err := input.ParseUltraStar(string(ustr))
	if err != nil {
		fmt.Fprintln(w, err)
		return
	}

	var buf bytes.Buffer
	if err := render.Render(song, &buf); err != nil {
		fmt.Fprintln(w, err)
		return
	}

	//Send the headers
	w.Header().Set("Content-Disposition", "attachment; filename=result.cdg")
	w.Header().Set("Content-Type", "application/octet-stream")
	io.Copy(w, &buf)
}
