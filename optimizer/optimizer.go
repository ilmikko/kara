package optimizer

import (
	"bytes"
	"gitlab.com/ilmikko/kara/instructions"
)

func New() *Optimizer {
	return &Optimizer{
		clut: map[bool][]byte{},
	}
}

type tile struct {
	pixels [12][6]byte
}

type Optimizer struct {
	tiles            [16][48]tile
	clut             map[bool][]byte
	hOffset, vOffset byte
}

func (o *Optimizer) Add(v instructions.Instruction) bool {
	switch i := v.(type) {
	case instructions.MemoryPreset:
		any := false
		for y := 0; 16 > y; y++ {
			for x := 0; 48 > x; x++ {
				for ty := 0; 12 > ty; ty++ {
					for tx := 0; 6 > tx; tx++ {
						if o.tiles[y][x].pixels[ty][tx] != i.Color {
							o.tiles[y][x].pixels[ty][tx] = i.Color
							any = true
						}
					}
				}
			}
		}
		return any
	case instructions.TileBlock:
		any := false
		for ty := 0; 12 > ty; ty++ {
			for tx := 0; 6 > tx; tx++ {
				nc := i.Color0
				if i.TilePixels[ty]&(1<<uint(tx)) > 0 {
					nc = i.Color1
				}
				if o.tiles[i.Row][i.Column].pixels[ty][tx] != nc {
					o.tiles[i.Row][i.Column].pixels[ty][tx] = nc
					any = true
				}
			}
		}
		return any
	case instructions.TileXOR:
		any := false
		for ty := 0; 12 > ty; ty++ {
			for tx := 0; 6 > tx; tx++ {
				nc := i.Color0
				if i.TilePixels[ty]&(1<<uint(tx)) > 0 {
					nc = i.Color1
				}
				if nc != 0 {
					o.tiles[i.Row][i.Column].pixels[ty][tx] ^= nc
					any = true
				}
			}
		}
		return any
	case instructions.Scroll:
		any := false
		if o.hOffset != i.HorizontalOffset {
			o.hOffset = i.HorizontalOffset
			any = true
		}
		if o.vOffset != i.VerticalOffset {
			o.vOffset = i.VerticalOffset
			any = true
		}
		xOffset := 0
		yOffset := 0
		var recolorRows, recolorColumns []byte
		switch i.Horizontal {
		case instructions.ScrollRight:
			xOffset = 1
			recolorColumns = append(recolorColumns, 0)
		case instructions.ScrollLeft:
			xOffset = -1
			recolorColumns = append(recolorColumns, 47)
		}
		switch i.Vertical {
		case instructions.ScrollDown:
			yOffset = 1
			recolorRows = append(recolorRows, 0)
		case instructions.ScrollUp:
			yOffset = -1
			recolorRows = append(recolorRows, 15)
		}
		buffer := o.tiles
		for y := 0; 16 > y; y++ {
			for x := 0; 48 > x; x++ {
				buffer[(y+yOffset+16)%16][(x+xOffset+48)%48] = o.tiles[y][x]
			}
		}
		o.tiles = buffer
		if i.IsPreset() {
			any = true
			for _, y := range recolorRows {
				for x := 0; 48 > x; x++ {
					for ty := 0; 12 > ty; ty++ {
						for tx := 0; 6 > tx; tx++ {
							o.tiles[y][x].pixels[ty][tx] = i.Color
						}
					}
				}
			}
			for _, x := range recolorColumns {
				for y := 0; 16 > y; y++ {
					for ty := 0; 12 > ty; ty++ {
						for tx := 0; 6 > tx; tx++ {
							o.tiles[y][x].pixels[ty][tx] = i.Color
						}
					}
				}
			}
		}
		if xOffset != 0 || yOffset != 0 {
			any = true
		}
		return any
	case instructions.LoadColorTable:
		nt := i.Data()
		if bytes.Equal(o.clut[i.High], nt) {
			return false
		}
		o.clut[i.High] = nt
		return true
	case instructions.Pause:
		return false
	default:
		return true
	}
}
