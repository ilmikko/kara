package render

import (
	"math"
	"io"

	"gitlab.com/ilmikko/kara/font"
	"gitlab.com/ilmikko/kara/input"
	"gitlab.com/ilmikko/kara/instructions"
	"gitlab.com/ilmikko/kara/scheduler"
)

func Char(char rune, x, y, scale float64) []instructions.Instruction {
	bg := byte(0)
	fg := byte(1)

	ins := []instructions.Instruction{}

	ft := font.GetChar(char)

	for py := 0; py < int(math.Ceil(scale)); py++ {
		for px := 0; px < int(math.Ceil(scale)); px++ {
			sl := make([]uint8, 12)

			for fy := 0; fy < 12; fy++ {
				row := uint8(0)

				// TODO(you): Clean this up.
				if py == int(math.Floor(scale)) {
					if float64(fy) >= 12 * math.Mod(scale, 1) {
						break
					}
				}

				for fx := 0; fx < 6; fx++ {
					if px == int(math.Floor(scale)) {
						if float64(fx) >= 6 * math.Mod(scale, 1) {
							break
						}
					}

					rx := uint8(8 - (float64(fx) / scale) - (float64(px) * (6.0 / scale)))
					ry := uint8((float64(fy) / scale) + (float64(py) * (12.0 / scale)))

					row |= (((ft[ry] >> rx) & 1) << uint8(5 - fx));
				}

				sl[fy] = row << 2;
			}

			ins = append(ins, instructions.TileBlock{bg, fg, byte(int(y + float64(py) + 1)), byte(int(x + float64(px) + 1)), sl});
		}
	}

	return ins;
}

func Line(line string, x, y, scale float64) []instructions.Instruction {
	ins := []instructions.Instruction{}

	for _, char := range line {
		ins = append(ins, Char(char, x, y, scale)...)
		x += scale
	}

	return ins;
}

func Render(song input.Song, out io.Writer) error {
	bg := byte(0)
	past := byte(1)
	future := byte(2)
	active := byte(3)

	palette := []instructions.RGB{
		{0x00, 0x00, 0x00},
		{0x3F, 0x3F, 0x3F},
		{0x3F, 0x00, 0x00},
		{0x3F, 0x3F, 0x00},
		{0x00, 0x3F, 0x3F},
		{0x3F, 0x00, 0x3F},
		{0x07, 0x00, 0x3F},
		{0x3F, 0x00, 0x07},
	}

	sched := scheduler.New()

	sched.AddAtStart(instructions.LoadColorTable{
		High:   false,
		Colors: palette,
	})
	sched.AddAtStart(instructions.LoadColorTable{
		High:   true,
		Colors: palette,
	})

	sched.AddAtStart(instructions.MemoryPreset{bg})

	for i, v := range song.Voices {
		renderSyllable := func(y, x int, s input.Syllable, renderDl int) {
			for i, c := range s.Text {
				sched.Add(instructions.TileBlock{bg, future, byte(y), byte(x + i), font.GetChar(c)}, renderDl-300, renderDl)
				sched.Add(instructions.TileBlock{bg, active, byte(y), byte(x + i), font.GetChar(c)}, s.FromTick-10, s.FromTick+2*i)
				sched.Add(instructions.TileBlock{bg, past, byte(y), byte(x + i), font.GetChar(c)}, s.TillTick-i, s.TillTick+10)
			}
		}
		clearTillEndOfLine := func(y, x int, renderDl int) {
			for ; 48 > x; x++ {
				sched.Add(instructions.TileBlock{bg, bg, byte(y), byte(x), font.GetChar(' ')}, renderDl-300, renderDl)
			}
		}
		nextRow := func(y int) int {
			if y%5 == 4 {
				return y - 3
			}
			return y + 1
		}
		y := i*5 + 1
		for _, l := range v {
			x := 1
			lineRenderDeadline := l[0][0].FromTick - 300
			for _, w := range l {
				wordLen := 0
				for _, s := range w {
					wordLen += len(s.Text)
				}
				if x+wordLen >= 48 && x > 1 {
					clearTillEndOfLine(y, x, lineRenderDeadline)
					y = nextRow(y)
					x = 1
				}
				for _, s := range w {
					renderSyllable(y, x, s, lineRenderDeadline)
					x += len(s.Text)
				}
				sched.Add(instructions.TileBlock{bg, future, byte(y), byte(x), font.GetChar(' ')}, lineRenderDeadline-300, lineRenderDeadline)
				x++
			}
			clearTillEndOfLine(y, x, lineRenderDeadline)
			y = nextRow(y)
		}
	}

	ins, err := sched.Schedule()
	if err != nil {
		panic(err)
	}
	for _, i := range ins {
		if _, err := out.Write(instructions.ToPacket(i)); err != nil {
			return err
		}
	}
	return nil
}
